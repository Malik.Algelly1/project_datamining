import setuptools

with open("README.rst", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="malikule",
    version="0.0.1",
    author="Malik Algelly, Samuel Simko",
    description="A cool library",
    long_description=long_description,
    long_description_content_type="text/x-rst",
    url="https://gitlab.unige.ch/Samuel.Simko/project_datamining",
    project_urls={
        "Bug Tracker": "https://gitlab.unige.ch/Samuel.Simko/project_datamining/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # package_dir={"malikule": "src",
    # "malikule.dataset": "src/dataset"},
    packages=["malikule", "malikule.dataset", "malikule.models"],
    python_requires=">=3.6",
)
