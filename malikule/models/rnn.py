import torch
import torch.nn as nn
import torch.nn.functional as F
import pytorch_lightning as pl


class RNN(pl.LightningModule):
    def __init__(
        self,
        num_features=33,
        lr=0.01,
        rnn_input_size=1,
        rnn_hidden_size=10,
        rnn_num_layers=2,
        len_smiles=10,
        use_lstm=False
    ):
        super().__init__()

        # Define loss function and learning rate
        self.loss = F.mse_loss
        self.lr = lr

        # Define net
        if use_lstm:
            self.rnn = nn.Sequential(
                nn.LSTM(input_size=rnn_input_size, dropout=0, hidden_size=rnn_hidden_size, batch_first=True, num_layers=rnn_num_layers),
            )
        else:
            self.rnn = nn.Sequential(
                nn.RNN(input_size=rnn_input_size, dropout=0, hidden_size=rnn_hidden_size, batch_first=True, num_layers=rnn_num_layers)
            )
        self.flatten = nn.Flatten()

        self.mlp = nn.Sequential(
            nn.Flatten(),
            nn.Linear(num_features-1, 50),
            nn.ReLU(),
            nn.Linear(50, 50),
            nn.ReLU(),
        )

        self.last_mlp = nn.Sequential(
            nn.Flatten(),
            nn.Linear(len_smiles * rnn_hidden_size + 50, 2)
        )

    def forward(self, x, z, y=None):
        x, _ = self.rnn(x)
        x = self.flatten(x),
        z = self.mlp(z)
        x = torch.cat((x[0], z), 1)
        x = self.last_mlp(x)
        return x

    def training_step(self, batch, batch_idx):
        x, z, y = batch
        y_hat = self.forward(x, z)
        loss = self.loss(y, y_hat)
        self.log("train_loss", loss)
        return {"loss": loss}

    def validation_step(self, batch, batch_idx):
        x, z, y = batch
        y_hat = self.forward(x, z)
        loss = self.loss(y, y_hat)
        self.log("val_loss", loss)
        return {"val_loss": loss}

    def test_step(self, batch, batch_idx):
        x, z, y = batch
        y_hat = self.forward(x, z)
        loss = self.loss(y, y_hat)
        self.log("test_loss", loss)
        return {"test_loss": loss}

    def predict_step(self, batch, batch_idx):
        x, z, y = batch
        y_hat = self.forward(x, z)
        return y_hat

    def configure_optimizers(self, step_size=1):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        scheduler = torch.optim.lr_scheduler.ExponentialLR(
            optimizer, gamma=0.99
        )

        return {
           'optimizer': optimizer,
           'lr_scheduler': scheduler,
           'monitor': 'val_loss'
        }
