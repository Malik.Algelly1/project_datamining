import torch
import torch.nn as nn
import torch.nn.functional as F
import pytorch_lightning as pl

class MLP(pl.LightningModule):
    def __init__(self, num_features, lr, num_hidden_layers=2, activation_function='relu', perceptrons_per_layer=3):
        super().__init__()
        # Define loss function and learning rate
        self.loss = F.mse_loss
        self.lr = lr
        if activation_function == 'relu':
            self.activation_function = nn.ReLU()
        elif activation_function == 'selu':
            self.activation_function = nn.SELU()
        elif activation_function == 'sigmoid':
            self.activation_function = nn.Sigmoid()
        else:
            raise NotImplementedError("Activation function not implemented")

        # Define net
        self.net = nn.Sequential(
            nn.Flatten(),
            nn.Linear(num_features, perceptrons_per_layer),
            *[nn.Sequential(
                # nn.BatchNorm1d(num_features=perceptrons_per_layer),
                # nn.Dropout(p=0.1),
                self.activation_function,
                nn.Linear(perceptrons_per_layer, perceptrons_per_layer),
            ) for _ in range(num_hidden_layers)
              ],
            # nn.BatchNorm1d(num_features=perceptrons_per_layer),
            self.activation_function,
            nn.Linear(perceptrons_per_layer, 2)
        )

    def forward(self, x, y=None):
        x = self.net(x)
        return x

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.forward(x)
        loss = self.loss(y, y_hat)
        self.log("train_loss", loss)
        return {"loss": loss}

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.forward(x)
        loss = self.loss(y, y_hat)
        self.log("val_loss", loss)
        return {"val_loss": loss}

    def test_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.forward(x)
        loss = self.loss(y, y_hat)
        self.log("test_loss", loss)
        return {"test_loss": loss}

    def predict_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.forward(x)
        return y_hat

    def configure_optimizers(self, step_size=1):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        scheduler = torch.optim.lr_scheduler.ExponentialLR(
            optimizer, gamma=0.9999
        )

        return {
           'optimizer': optimizer,
           'lr_scheduler': scheduler,
           'monitor': 'val_loss'
        }
