import sklearn
import numpy as np
import pandas as pd
import pytorch_lightning as pl

import torch
from torch.utils.data.dataloader import DataLoader

from torch.utils.data import DataLoader, Dataset

from .utils import get_smiles_encodings, smile_to_hot


class MoleculeDataset(Dataset):
    """Molecule Dataset"""

    def __init__(self, X, y, use_one_hot=False):
        self.use_one_hot = use_one_hot

        if use_one_hot:
            self.X = X["SMILES_Encoded"].to_numpy()
            self.Z = X.loc[:, X.columns != "SMILES_Encoded"].to_numpy()
            self.y = y.to_numpy()
        else:
            self.X = X.to_numpy()
            self.y = y.to_numpy()

    def __len__(self):
        return self.X.shape[0]

    def __getitem__(self, idx):
        if self.use_one_hot:
            return (
                torch.tensor(self.X[idx], dtype=torch.float32),
                torch.tensor(self.Z[idx], dtype=torch.float32),
                torch.tensor(self.y[idx], dtype=torch.float32),
            )

        return (
            torch.tensor(self.X[idx], dtype=torch.float32),
            torch.tensor(self.y[idx], dtype=torch.float32),
        )


class MoleculeDataModule(pl.LightningDataModule):
    """Molecule Datamodule"""

    def __init__(
        self,
        path_of_csv,
        test_size=0.3,
        random_state=0,
        batch_size=16,
        use_kfold=False,
        use_one_hot=False,
        n_splits=10,
    ):
        super().__init__()
        self.data = pd.read_csv(path_of_csv)
        self.use_kfold = use_kfold
        self.use_one_hot = use_one_hot

        if use_kfold:
            self.n_splits = n_splits
            self.kfold = sklearn.model_selection.KFold(
                n_splits=self.n_splits, shuffle=True, random_state=0
            )

        # Remove rows with NaN values
        self.data = self.data.dropna()

        # Remove duplicates
        self.data = self.data.sort_values(by=["Energy_(kcal/mol)"])
        self.data = self.data.drop_duplicates(subset=["Chiral_Molecular_SMILES"])
        self.data = self.data.sort_index()

        # self.data = self.data.drop(self.data.columns[self.data.isnull().any()])
        self.data = self.data.drop(
            list(self.data.columns[self.data.isnull().any()]), axis=1
        )
        print("Removed columns with NaN in them")

        self.batch_size = batch_size
        self.random_state = random_state
        self.test_size = test_size

        y = self.data[["Energy DG:kcal/mol)", "Energy_(kcal/mol)"]]

        if use_one_hot:

            _, alphabet, largest_smile_len = get_smiles_encodings(path_of_csv)
            self.alphabet = alphabet
            self.largest_smile_len = largest_smile_len
            test = [
                smile_to_hot(smile, largest_smile_len, alphabet)[1]
                for smile in self.data["Chiral_Molecular_SMILES"]
            ]

            X = self.data.drop(
                columns=[
                    "Energy DG:kcal/mol)",
                    "Energy_(kcal/mol)",
                    "Chiral_Molecular_SMILES",
                ]
            )

            self.X_mean, self.X_std = X.mean(), X.std()
            # Normalize data
            self.X = (X - X.mean()) / X.std()

            self.X["SMILES_Encoded"] = test

            self.y = self.data[["Energy DG:kcal/mol)", "Energy_(kcal/mol)"]]

        else:

            # Make attributes consisting of the number of occurences
            # of characters in the Chiral Molecular SMILES, for all characters
            # possible. (This is a very simple tokenization)
            symbols = np.unique(list("".join(self.data["Chiral_Molecular_SMILES"])))
            symbols.sort()
            for symbol in symbols:
                if symbol in list("""\\^$.|?*+()[]\\{\\}"""):
                    self.data["Count of {}".format(symbol)] = self.data[
                        "Chiral_Molecular_SMILES"
                    ].str.count("\\" + symbol)
                else:
                    self.data["Count of {}".format(symbol)] = self.data[
                        "Chiral_Molecular_SMILES"
                    ].str.count(symbol)

            X = self.data.drop(
                columns=[
                    "Energy DG:kcal/mol)",
                    "Energy_(kcal/mol)",
                    "Chiral_Molecular_SMILES",
                ]
            )

            self.X_mean, self.X_std = X.mean(), X.std()
            # Normalize data
            self.X = (X - X.mean()) / X.std()

        self.y_mean, self.y_std = y.mean(), y.std()
        self.y = (y - y.mean()) / y.std()

        self.num_features = self.X.shape[-1]

        self.X.reset_index(drop=True, inplace=True)
        print("Shape of data", self.X.shape)

    def setup_folds(self, fold_index: int) -> None:
        (
            self.X_train,
            self.X_test,
            self.y_train,
            self.y_test,
        ) = sklearn.model_selection.train_test_split(
            self.X, self.y, test_size=self.test_size, random_state=0
        )
        self.X_train.reset_index(drop=True, inplace=True)
        self.y_train.reset_index(drop=True, inplace=True)
        self.X_test.reset_index(drop=True, inplace=True)
        self.y_test.reset_index(drop=True, inplace=True)

        self.kfolds = list(self.kfold.split(self.X_train))
        self.X_validation = self.X_train.iloc[np.array(self.kfolds[fold_index][1])]
        self.X_train = self.X_train.iloc[np.array(self.kfolds[fold_index][0])]
        self.y_validation = self.y_train.iloc[np.array(self.kfolds[fold_index][1])]
        self.y_train = self.y_train.iloc[np.array(self.kfolds[fold_index][0])]

        print("FOLD {}".format(fold_index))

    def setup(self, stage=None):

        if not self.use_kfold:
            (
                self.X_train,
                self.X_test,
                self.y_train,
                self.y_test,
            ) = sklearn.model_selection.train_test_split(
                self.X, self.y, test_size=self.test_size, random_state=self.random_state
            )

            self.train_dataset = MoleculeDataset(self.X_train, self.y_train, use_one_hot=self.use_one_hot)
            self.validation_dataset = MoleculeDataset(self.X_test, self.y_test, use_one_hot=self.use_one_hot)
            print("Not using KFold")

        else:
            self.train_dataset = MoleculeDataset(self.X_train, self.y_train, use_one_hot=self.use_one_hot)
            self.validation_dataset = MoleculeDataset(
                self.X_validation, self.y_validation, use_one_hot=self.use_one_hot
            )
            self.test_dataset = MoleculeDataset(self.X_test, self.y_test, use_one_hot=self.use_one_hot)

    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size=self.batch_size)

    def val_dataloader(self):
        return DataLoader(self.validation_dataset, batch_size=self.batch_size)

    def test_dataloader(self):
        return DataLoader(self.test_dataset, batch_size=self.batch_size)
