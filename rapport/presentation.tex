\documentclass{beamer}
\usepackage{verbatim}
\usepackage{minted}
\usepackage{pythonhighlight}
\usepackage{listings}

% \usetheme{Boadilla}
\usetheme{boxes}

\title{Data Mining Project}
\subtitle{Electronic Energy Prediction methods from molecular data}
\author{Malik Algelly, Samuel Šimko}
\institute{University of Geneva}
\date{June 24 2022}
\logo{
	\includegraphics[width=2cm]{images/unige.png}
}

\begin{document}

\begin{frame}
	\titlepage
\end{frame}

\begin{frame}
	\frametitle{Outline}

	\tableofcontents
\end{frame}

\begin{frame}
	\section{Objective}
	\frametitle{Objective}

	From a dataset of molecular data, predict the \textit{Energy\_} and \textit{Energy\_DG} attributes.

	\begin{itemize}
		\item Understanding the data, preprocess the data;
		\item Implement three different algorithms and a baseline;
		\item Find the best hyperparameters for each of the algorithms using the testing set;
		\item Use the best models for each algorithm on the testing dataset;
		\item Compare the performances
	\end{itemize}
\end{frame}

\begin{frame}
	\section{What we've built}
	\frametitle{What we've built}

	\begin{itemize}
		\item A Python library (\textit{Malikule}) which one can install with a simple \textit{pip install .};
		\item Classes to preprocess the data, build training and testing sets;
		\item  Four different algorithms:
			\begin{itemize}
				\item Linear Regression (scikit-learn);
				\item Support Vector Regression (scikit-learn);
				\item Multi Layer Perceptron (PyTorch);
				\item Recurrent Neural Networks (PyTorch).
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\section{Data processing}
	\frametitle{Data processing}
	\begin{itemize}
		\item Analysis through Linear Regression
		\item SMILES String encoding:
			\begin{itemize}
				\item For each symbol, the number of occurences in the string;
				\item One-Hot Encoding using \emph{utils.py} file.
			\end{itemize}
	\end{itemize}
\end{frame}

\section{Algorithms used}
\begin{frame}
	\subsection{Linear Regression}
	\frametitle{Linear Regression}

	\begin{itemize}
		\item Baseline algorithm
		\item Good result with the selected data set
		\item No hyperparameter
		\item Lasso regularizer
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Linear Regression (Cont'd)}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.7\textwidth]{images/Lasso_X_with_SMILES_energy_50.png}
		\caption{Optimization of the Lasso model with the set containing all the information for Energy prediction. 100 trials for $\alpha$ ranging from 0.001 to 10}
		\label{fig:Lasso1}
	\end{figure}
\end{frame}


\begin{frame}
	\subsection{Support Vector Regression}
	\frametitle{Support Vector Regression}

	\begin{itemize}
		\item Ability to explain non-linear data
		\item Three hyperparameters
		\item Less effective than linear regression
	\end{itemize}

\end{frame}

\begin{frame}
	\frametitle{Support Vector Regression (Cont'd)}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.7\textwidth]{images/SVR_X with SMILES_energy_50.png}
		\caption{Optimization of the SVR model with the set containing all the information for Energy prediction.
		1000 trials for $C$ from 1 to 100, $\epsilon$ from 0 to 1 and $\gamma$ from 0 to 10}
		\label{fig:SVR1}
	\end{figure}
\end{frame}

\begin{frame}
	\subsection{Multi-Layer Perceptrons}
	\frametitle{Multi-Layer Perceptrons}

	\begin{itemize}
		\item Written in \emph{PyTorch} and \emph{PyTorch\_Lightning}
		\item More intuitive launch syntax than simple PyTorch
	\end{itemize}
	\vspace*{0.5cm}
	\small
	\inputminted{python}{code.py}
\end{frame}

\begin{frame}
	\frametitle{Multi-Layer Perceptrons (Cont'd)}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.14\textwidth]{images/mlp_drawio.png}
		\caption{An MLP from our package}
		\label{fig:mlp}
	\end{figure}

\end{frame}

\begin{frame}
	\subsection{Recurrent Neural Networks}
	\frametitle{Recurrent neural networks}

	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.35\textwidth]{images/rnn_drawio.png}
		\caption{A Recurrent neural network from our package}
		\label{fig:rnn}
	\end{figure}

\end{frame}

\begin{frame}
	\frametitle{Recurrent neural networks (Cont'd)}

	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.5\textwidth]{images/rnn_cell.png}
		\caption{A RNN cell}
		\label{fig:rnn_cell}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Recurrent neural networks (Cont'd)}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.8\textwidth]{images/ltsm.png}
		\caption{An LSTM cell}
		\label{fig:lstm_cell}
	\end{figure}
\end{frame}

\begin{frame}
	\section{Results}
	\frametitle{Results}

	\begin{figure}[htpb]
		\centering
		\begin{tabular}{|c|c|c|}
			\hline
			Algorithm & Testing loss \\
			\hline
			Linear Regression (Baseline) & 2.64e-06 \\
			\hline
			SVR & 4.78e-05 \\
			\hline
			MLP & 7.09e-04 \\
			\hline
			RNN & 6.39e-04 \\
			\hline
		\end{tabular}
		\caption{Testing losses for each algorithm used}
		\label{fig:tabloss}
	\end{figure}

\end{frame}

\begin{frame}
	\frametitle{Hypothesis testing}
	\begin{itemize}
		\item We tested if the distributions $SVR(X_{test})$ and $LinReg(X_{test})$ had the same mean by doing a paired t-test.
		\item We wanted to test if the distributions $(SVR(X_{test}) - y_{test})^2$ and  $(LinReg(X_{test}) - y_{test})^2$ had the same mean.
		\item The data in the hypothesis in this case is not normally distributed anymore, therefore we cannot use paired t-test.
		\item Maybe we could have used Wilcoxon test (generalized paired t-test) instead ?
	\end{itemize}
\end{frame}

\begin{frame}
	\section{What we did not have time to do}
	\frametitle{What we did not have time to do}

	\begin{itemize}
		\item Launch the Optuna hyperparameter optimization on the Yggdrasil cluster;
		\item Fix the \textit{requirements.py} file;
		\item Use readthedocs.io to provide online documentation;
		\item Add more sections to the rapport.
	\end{itemize}
\end{frame}

\begin{frame}
	\centering
	Thank you !
	\vspace{1cm}

	% \url{https://malikule.readthedocs.io}
\end{frame}
\end{document}
