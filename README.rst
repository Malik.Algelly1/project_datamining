==================
Malikule
==================

About
-----

This repository is the home of the final project of the 2022 Data Mining lecture of the University of Geneva.


Installation
------------

.. code-block:: console

   $ pip install -r requirements.txt
   $ pip install --user .


Documentation
-------------

The documentation is not yet available.
